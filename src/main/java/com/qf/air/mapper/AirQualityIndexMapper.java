package com.qf.air.mapper;

import com.qf.air.entity.AirQualityIndex;
import com.qf.air.entity.AirQualityIndexExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AirQualityIndexMapper {
    long countByExample(AirQualityIndexExample example);

    int deleteByExample(AirQualityIndexExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(AirQualityIndex record);

    int insertSelective(AirQualityIndex record);

    List<AirQualityIndex> selectByExample(AirQualityIndexExample example);

    AirQualityIndex selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") AirQualityIndex record, @Param("example") AirQualityIndexExample example);

    int updateByExample(@Param("record") AirQualityIndex record, @Param("example") AirQualityIndexExample example);

    int updateByPrimaryKeySelective(AirQualityIndex record);

    int updateByPrimaryKey(AirQualityIndex record);

    /**
     * 根据air的id查询空气质量信息和区域名称
     * @param id
     * @return
     */
    AirQualityIndex findById(Integer id);

    /**
     * 根据air的district_id查询空气质量信息和区域名称
     * @param districtId
     * @return
     */
    List<AirQualityIndex> findByDistrictId(Integer districtId);
}