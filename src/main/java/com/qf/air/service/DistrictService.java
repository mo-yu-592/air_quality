package com.qf.air.service;

import com.qf.air.entity.District;

import java.util.List;


public interface DistrictService {

    /**
     * 查询全部区域信息
     * @return
     */
    List<District> findAll();

}
