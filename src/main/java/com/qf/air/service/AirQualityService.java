package com.qf.air.service;

import com.github.pagehelper.PageInfo;
import com.qf.air.entity.AirQualityIndex;


public interface AirQualityService {

    /**
     * 添加空气质量信息
     * @param airQualityIndex
     */
    void insert(AirQualityIndex airQualityIndex);

    /**
     * 根据id修改空气质量信息
     * @param airQualityIndex
     */
    void updateById(AirQualityIndex airQualityIndex);

    /**
     * 根据id删除空气质量信息
     * @param id   主键id
     */
    void deleteById(Integer id);


    /**
     * 根据主键id查询空气质量信息
     * @param id   主键id
     * @return
     */
    AirQualityIndex findById(Integer id);


    /**
     * 根据区域id分页查询空气质量信息
     * @param page    当前页
     * @param rows    每页显示行
     * @param districtId    区域id
     * @return
     */
    PageInfo<AirQualityIndex> findByDistrictIdAndPage(Integer page, Integer rows, Integer districtId);

}
