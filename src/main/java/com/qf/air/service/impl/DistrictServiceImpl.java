package com.qf.air.service.impl;

import com.qf.air.entity.District;
import com.qf.air.mapper.DistrictMapper;
import com.qf.air.service.DistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DistrictServiceImpl implements DistrictService {

    @Autowired
    private DistrictMapper districtMapper;

    @Override
    public List<District> findAll() {
        return districtMapper.selectByExample(null);
    }
}
