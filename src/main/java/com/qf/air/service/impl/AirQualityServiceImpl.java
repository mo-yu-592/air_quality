package com.qf.air.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qf.air.entity.AirQualityIndex;
import com.qf.air.mapper.AirQualityIndexMapper;
import com.qf.air.service.AirQualityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class AirQualityServiceImpl implements AirQualityService {

    @Autowired
    private AirQualityIndexMapper airQualityIndexMapper;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void insert(AirQualityIndex airQualityIndex) {
        int i = airQualityIndexMapper.insertSelective(airQualityIndex);
        if(i != 1){
            System.out.println("【添加空气质量信息】  添加失败！");
            throw new RuntimeException("【添加空气质量信息】  添加失败！");
        }
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void updateById(AirQualityIndex airQualityIndex) {
        int i = airQualityIndexMapper.updateByPrimaryKeySelective(airQualityIndex);
        if(i != 1){
            System.out.println("【修改空气质量信息】  修改失败！");
            throw new RuntimeException("【修改空气质量信息】  修改失败！");
        }
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void deleteById(Integer id) {
        int i = airQualityIndexMapper.deleteByPrimaryKey(id);
        if(i != 1){
            System.out.println("【删除空气质量信息】  删除失败！");
            throw new RuntimeException("【删除空气质量信息】  删除失败！");
        }
    }

    @Override
    public AirQualityIndex findById(Integer id) {
        return airQualityIndexMapper.findById(id);
    }

    @Override
    public PageInfo<AirQualityIndex> findByDistrictIdAndPage(Integer page, Integer rows, Integer districtId) {
        //1. 分页
        PageHelper.startPage(page,rows);

        //2. 查询
        List<AirQualityIndex> airList = airQualityIndexMapper.findByDistrictId(districtId);

        //3. 封装PageInfo
        PageInfo<AirQualityIndex> pageInfo = new PageInfo<>(airList);

        //4. 返回
        return pageInfo;
    }
}
