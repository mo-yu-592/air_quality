package com.qf.air.service;

import com.qf.air.entity.District;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {
        "classpath:applicationContext-dao.xml",
        "classpath:applicationContext-service.xml"
})
public class DistrictServiceTest {

    @Autowired
    private DistrictService districtService;

    @Test
    public void findAll() {
        List<District> list = districtService.findAll();
        // 断言……
        Assert.assertEquals(6,list.size());
        for (District district : list) {
            System.out.println(district);
        }
    }
}