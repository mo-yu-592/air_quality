package com.qf.air.service;

import com.github.pagehelper.PageInfo;
import com.qf.air.entity.AirQualityIndex;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;



@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {
        "classpath:applicationContext-dao.xml",
        "classpath:applicationContext-service.xml"
})
public class AirQualityServiceTest {

    @Autowired
    private AirQualityService airQualityService;

    @Test
    @Transactional
    public void insert() {
        AirQualityIndex airQualityIndex = new AirQualityIndex();
        airQualityIndex.setDistrictId(5);
        airQualityIndex.setMonitorTime(new Date());
        airQualityIndex.setPm10(50);
        airQualityIndex.setPm25(88);
        airQualityIndex.setMonitoringStation("测试监测站");

        airQualityService.insert(airQualityIndex);
    }

    @Test
    @Transactional
    public void updateById() {
        AirQualityIndex airQualityIndex = new AirQualityIndex();
        airQualityIndex.setId(1);
        airQualityIndex.setDistrictId(5);
        airQualityIndex.setMonitorTime(new Date());
        airQualityIndex.setPm10(50);
        airQualityIndex.setPm25(88);
        airQualityIndex.setMonitoringStation("测试监测站");

        airQualityService.updateById(airQualityIndex);
    }

    @Test
    @Transactional
    public void deleteById() {
        airQualityService.deleteById(6);
    }







    @Test
    public void findById() {
    }

    @Test
    public void findByDistrictIdAndPage() {
        PageInfo<AirQualityIndex> pageInfo = airQualityService.findByDistrictIdAndPage(1, 5, null);
        for (AirQualityIndex airQualityIndex : pageInfo.getList()) {
            System.out.println(airQualityIndex);
        }
    }
}