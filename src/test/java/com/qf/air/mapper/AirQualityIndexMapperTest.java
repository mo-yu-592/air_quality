package com.qf.air.mapper;

import com.qf.air.entity.AirQualityIndex;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {
        "classpath:applicationContext-dao.xml"
})
public class AirQualityIndexMapperTest {

    @Autowired
    private AirQualityIndexMapper mapper;

    @Test
    public void findById() {
        AirQualityIndex air = mapper.findById(1);
        System.out.println(air);
        System.out.println(air.getDistrict());
    }


    @Test
    public void findByDistrictId() {
        List<AirQualityIndex> list = mapper.findByDistrictId(null);
        for (AirQualityIndex airQualityIndex : list) {
            System.out.println(airQualityIndex);
            System.out.println(airQualityIndex.getDistrict());
        }
    }
}